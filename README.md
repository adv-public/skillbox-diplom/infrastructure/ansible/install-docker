# Web Server denav.net

This folder contains a configuration that deploys a cluster of web servers 
(using [EC2](https://aws.amazon.com/ec2/) and [Auto Scaling](https://aws.amazon.com/autoscaling/)) and a load balancer
(using [NLB](https://aws.amazon.com/elasticloadbalancing/)) in an [Amazon Web Services (AWS) 
account](http://aws.amazon.com/). The load balancer listens on port 80. 
Provides a Hetzner DNS Records resource to create, update and delete DNS Records[Terraform Registory](https://registry.terraform.io/providers/timohirt/hetznerdns/2.2.0).

## Pre-requisites

* You must have [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) installed on your computer.

## Quick start

Config the Web-Server with Ansible:

Ansible code one that deploys everything you need to run service - docker and related.
```
cd /ansible
ansible-playbook playbooks/pl_infra_docker.yml
```

When the `apply` command completes, it will output the DNS name of the load balancer. To test the load balancer:

```
curl http://<nlb_dns_name>/
or 
curl http://test.denav.net/
```

Clean up when you're done:

```
terraform destroy
```